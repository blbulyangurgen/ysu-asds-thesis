from torch.utils.data import Dataset
import torch

import os
import pickle


class VisionTextEvaluateDataset(Dataset):
    def __init__(self, data_path, split_type, tokenizer, max_target_length=64, num_examples=None):
        self.imgs = torch.load(os.path.join(data_path, f'{split_type}_set.pt'))

        with open(os.path.join(data_path, f'{split_type}_sents'), 'rb') as fp:
            self.texts = pickle.load(fp)

        if num_examples is not None:
            self.imgs = self.imgs[:num_examples]
            self.texts = self.texts[:num_examples]

        self.tokenizer = tokenizer
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        text = self.texts[idx]
        labels = self.tokenizer(
            text,
            padding='max_length',
            max_length=self.max_target_length,
            truncation=True,
        ).input_ids
        labels = torch.tensor(labels)

        encoding = {'pixel_values': self.imgs[idx], 'labels': labels}

        return encoding
