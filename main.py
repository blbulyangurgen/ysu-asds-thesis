import os
import json

from transformers import AutoTokenizer, VisionEncoderDecoderModel

from argument_parser import ArgumentParser
from dataloader import VisionTextDataset
from evaluate_dataloader import VisionTextEvaluateDataset
from model import VLM
from trainer import Trainer
from train import Train
from evaluate import Evaluate


argument_parser = ArgumentParser()
args = argument_parser.call()

assert args.mode in ['train', 'test'], 'invalid mode argument'

if __name__ == '__main__':
    print(f'{args.mode.capitalize()}ing with the following arguments\n')
    print(json.dumps(vars(args), indent=4))

    output_dir = os.path.join(args.output_dir, args.model_name)

    os.makedirs(output_dir, exist_ok=True)

    tokenizer = AutoTokenizer.from_pretrained(args.decoder_model_name)

    if args.mode == 'train':
        vlm = VLM(
            tokenizer=tokenizer,
            encoder_model_name=args.encoder_model_name,
            decoder_model_name=args.decoder_model_name,
            num_encoder_fine_tuned_layers=args.num_encoder_fine_tuned_layers,
            decoder_frozen_layer_range=args.decoder_frozen_layer_range,
            train_cross_attention=args.train_cross_attention,
        )
        model = vlm.call()
    else:
        model = VisionEncoderDecoderModel.from_pretrained(args.evaluate_from_checkpoint)

        if not tokenizer.pad_token:
            tokenizer.add_special_tokens({'pad_token': '[PAD]'})
            model.decoder.resize_token_embeddings(len(tokenizer))

    if args.mode == 'train':
        train_dataset = VisionTextDataset(
            data_path=args.data_path,
            split_type='train',
            tokenizer=tokenizer,
            max_target_length=args.max_target_length,
            num_examples=args.num_examples,
        )
        eval_dataset = VisionTextDataset(
            data_path=args.data_path,
            split_type='valid',
            tokenizer=tokenizer,
            max_target_length=args.max_target_length,
            num_examples=args.num_examples,
        )

        trainer = Trainer(
            model=model,
            tokenizer=tokenizer,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            output_dir=output_dir,
            batch_size=args.batch_size,
            num_train_epochs=args.num_train_epochs,
            warmup_ratio=args.warmup_ratio,
            metric_for_best_model=args.metric_for_best_model,
            greater_is_better=args.greater_is_better,
            learning_rate=args.learning_rate,
        )
        seq_2_seq_trainer = trainer.call()

        train = Train(trainer=seq_2_seq_trainer, resume_from_checkpoint=args.resume_from_checkpoint)
        result = train.call()
    else:
        test_dataset = VisionTextEvaluateDataset(
            args.test_images_data_path,
            args.test_sentences_data_path,
            tokenizer,
            max_target_length=args.max_target_length,
            num_examples=args.num_examples,
        )

        evaluate = Evaluate(
            model=model,
            tokenizer=tokenizer,
            eval_dataset=test_dataset,
            batch_size=args.batch_size,
            max_target_length=args.max_target_length,
        )
        result = evaluate.call()

    print(f'{args.mode.capitalize()}ing results\n')
    print(result)
