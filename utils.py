from transformers import ViTFeatureExtractor
import torchvision
import torchvision.transforms.functional as fn
import torch as th
import os
import pickle


def video2image_from_path(video_path, feature_extractor_name='google/vit-base-patch32-224-in21k'):
    video = torchvision.io.read_video(video_path)

    return video2image(video[0], feature_extractor_name)


def video2image(video, feature_extractor_name='google/vit-base-patch32-224-in21k'):
    feature_extractor = ViTFeatureExtractor.from_pretrained(
        feature_extractor_name
    )

    vid = th.permute(video, (3, 0, 1, 2))
    samp = th.linspace(0, vid.shape[1]-1, 49, dtype=th.long)
    vid = vid[:, samp, :, :]

    im_l = list()
    for i in range(vid.shape[1]):
        im_l.append(vid[:, i, :, :])

    inputs = feature_extractor(im_l, return_tensors="pt")

    inputs = inputs['pixel_values']

    im_h = list()
    for i in range(7):
        im_v = th.cat((inputs[0+i*7, :, :, :],
                       inputs[1+i*7, :, :, :],
                       inputs[2+i*7, :, :, :],
                       inputs[3+i*7, :, :, :],
                       inputs[4+i*7, :, :, :],
                       inputs[5+i*7, :, :, :],
                       inputs[6+i*7, :, :, :]), 2)
        im_h.append(im_v)
    resize = fn.resize(th.cat(im_h, 1), size=[224])

    return resize

def combine_datasets(data_path, split_type, num_examples):
    imgs = []
    texts = []

    data_paths = [d[0] for d in os.walk(os.path.join(data_path)) if not os.path.join(data_path) == d[0]]

    for path in data_paths:
        split_imgs = th.load(os.path.join(path, f'{split_type}_set.pt'))

        with open(os.path.join(path, f'{split_type}_sents'), 'rb') as fp:
            split_texts = pickle.load(fp)

        imgs.append(split_imgs)
        texts += split_texts

    imgs = th.cat(imgs)

    if num_examples is not None:
        imgs = imgs[:num_examples]
        texts = texts[:num_examples]

    return imgs, texts
