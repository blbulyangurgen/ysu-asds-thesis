from torch.utils.data import Dataset
import torch

import utils


class VisionTextDataset(Dataset):
    def __init__(self, data_path, split_type, tokenizer, max_target_length=64, num_examples=None):
        self.imgs, self.texts = utils.combine_datasets(
            data_path,
            split_type,
            num_examples
        )

        self.tokenizer = tokenizer
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        rand_int = torch.randint(low=0, high=len(self.texts[idx]), size=(1,))
        text = self.texts[idx][rand_int]
        labels = self.tokenizer(
            text,
            padding='max_length',
            max_length=self.max_target_length,
            truncation=True,
        ).input_ids
        # important: make sure that PAD tokens are ignored by the loss function
        labels = [label if label !=
                  self.tokenizer.pad_token_id else -100 for label in labels]

        encoding = {
            'pixel_values': self.imgs[idx], 'labels': torch.tensor(labels)}

        return encoding
