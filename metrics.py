from datasets import load_metric


class Metrics:
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.meteor_metric = load_metric('meteor')

    def call(self):
        def compute_metrics(pred):
            labels_ids = pred.label_ids
            pred_ids = pred.predictions

            predictions = self.tokenizer.batch_decode(
                pred_ids, skip_special_tokens=True
            )
            labels_ids[labels_ids == -100] = self.tokenizer.pad_token_id
            references = self.tokenizer.batch_decode(
                labels_ids, skip_special_tokens=True
            )

            meteor = self.meteor_metric.compute(
                predictions=predictions, references=references
            )

            metrics = {
                'meteor': meteor['meteor'],
            }

            return metrics

        return compute_metrics
