from transformers import Seq2SeqTrainer, Seq2SeqTrainingArguments
from transformers import default_data_collator

from metrics import Metrics


class Trainer:
    def __init__(self, output_dir, batch_size, num_train_epochs, warmup_ratio, model, train_dataset, eval_dataset, tokenizer, metric_for_best_model, greater_is_better, learning_rate):
        self.output_dir = output_dir
        self.batch_size = batch_size
        self.num_train_epochs = num_train_epochs
        self.warmup_ratio = warmup_ratio
        self.metric_for_best_model = metric_for_best_model
        self.greater_is_better = greater_is_better
        self.learning_rate = learning_rate

        self.model = model
        self.train_dataset = train_dataset
        self.eval_dataset = eval_dataset

        self.training_args = self.__get_training_args()

        self.compute_metrics = Metrics(tokenizer).call()

    def call(self):
        trainer = Seq2SeqTrainer(
            model=self.model,
            args=self.training_args,
            compute_metrics=self.compute_metrics,
            train_dataset=self.train_dataset,
            eval_dataset=self.eval_dataset,
            data_collator=default_data_collator,
        )

        return trainer

    def __get_training_args(self):
        return Seq2SeqTrainingArguments(
            output_dir=self.output_dir,
            predict_with_generate=True,
            evaluation_strategy='epoch',
            per_device_train_batch_size=self.batch_size,
            per_device_eval_batch_size=self.batch_size,
            learning_rate=self.learning_rate,
            weight_decay=0,
            num_train_epochs=self.num_train_epochs,
            lr_scheduler_type='linear',
            warmup_ratio=self.warmup_ratio,
            logging_strategy='epoch',
            save_strategy='epoch',
            load_best_model_at_end=True,
            metric_for_best_model=self.metric_for_best_model,
            greater_is_better=self.greater_is_better,
            save_total_limit=2,
        )
