import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
from datasets import load_metric


class Evaluate:
    def __init__(self, model, tokenizer, eval_dataset, batch_size, max_target_length):
        self.model = model
        self.tokenizer = tokenizer
        self.batch_size = batch_size
        self.max_target_length = max_target_length

        self.eval_dataloader = DataLoader(
            eval_dataset,
            batch_size=self.batch_size,
            shuffle=False,
        )
        self.meteor_metric = load_metric('meteor')

    def call(self):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.model.to(device)

        for data in tqdm(self.eval_dataloader):
            actual_texts_list = self.tokenizer.batch_decode(
                sequences=data['labels'].reshape(-1, self.max_target_length),
                skip_special_tokens=True,
            )
            actual_texts_list_of_lists = [
                actual_texts_list[idx:idx + 20] for idx in range(0, len(actual_texts_list), 20)
            ]

            generated_ids = self.model.generate(data['pixel_values'].to(device))
            generated_texts = self.tokenizer.batch_decode(
                sequences=generated_ids,
                skip_special_tokens=True,
            )

            self.meteor_metric.add_batch(
                predictions=generated_texts,
                references=actual_texts_list_of_lists,
            )

        meteor_result = self.meteor_metric.compute()

        evaluation_results = {
            'meteor': meteor_result['meteor']
        }

        return evaluation_results
