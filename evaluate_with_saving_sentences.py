from evaluate_dataloader import VisionTextEvaluateDataset
import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
from datasets import load_metric
import pickle
import argparse

from transformers import AutoTokenizer, VisionEncoderDecoderModel


class Evaluate:
    def __init__(self, model, tokenizer, eval_dataset, batch_size, max_target_length, saving_path):
        self.model = model
        self.tokenizer = tokenizer
        self.batch_size = batch_size
        self.max_target_length = max_target_length

        self.eval_dataloader = DataLoader(
            eval_dataset,
            batch_size=self.batch_size,
            shuffle=False,
        )
        self.meteor_metric = load_metric('meteor')

        self.saving_path = saving_path

    def call(self):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.model.to(device)

        sents=list()
        for data in tqdm(self.eval_dataloader):
            actual_texts_list = self.tokenizer.batch_decode(
                sequences=data['labels'].reshape(-1, self.max_target_length),
                skip_special_tokens=True,
            )
            actual_texts_list_of_lists = [
                actual_texts_list[idx:idx + 20] for idx in range(0, len(actual_texts_list), 20)
            ]

            generated_ids = self.model.generate(data['pixel_values'].to(device))
            generated_texts = self.tokenizer.batch_decode(
                sequences=generated_ids,
                skip_special_tokens=True,
            )

            self.meteor_metric.add_batch(
                predictions=generated_texts,
                references=actual_texts_list_of_lists,
            )
            
            sents+=generated_texts
            
        with open(self.saving_path, "wb") as fp:   
            pickle.dump(sents, fp)
            
        meteor_result = self.meteor_metric.compute()

        evaluation_results = {
            'meteor': meteor_result['meteor']
        }

        return evaluation_results


parser = argparse.ArgumentParser()
parser.add_argument(
    '--decoder_model_name',
    type=str,
    default='bert-base-cased',
    help='Indicates decoder model name in vision encoder decoder model.'
)
parser.add_argument(
    '--max_target_length',
    type=int,
    default=64,
    help='Indicates maximum target length in tokenizer.'
)
parser.add_argument(
    '--batch_size',
    type=int,
    default=30,
    help='Indicates batch size.'
)
parser.add_argument(
    '--num_examples',
    type=int,
    default=None,
    help='Indicates number of examples to be used.'
)
parser.add_argument(
    '--data_path',
    type=str,
    default='data/msr-vtt',
    help='Indicates data path.'
)
parser.add_argument(
    '--evaluate_from_checkpoint',
    type=str,
    help='Indicates checkpoint path that model should be loaded from for evaluation.'
)
parser.add_argument(
    '--saving_path',
    type=str,
    required=True,
    help='Indicates data path.'
)

args = parser.parse_args()

tokenizer = AutoTokenizer.from_pretrained(args.decoder_model_name)

model = VisionEncoderDecoderModel.from_pretrained(args.evaluate_from_checkpoint)

if not tokenizer.pad_token:
    tokenizer.add_special_tokens({'pad_token': '[PAD]'})
    model.decoder.resize_token_embeddings(len(tokenizer))

test_dataset = VisionTextEvaluateDataset(
    data_path=args.data_path,
    split_type='test',
    tokenizer=tokenizer,
    max_target_length=args.max_target_length,
    num_examples=args.num_examples,
)

eval = Evaluate(
    model=model,
    tokenizer=tokenizer,
    eval_dataset=test_dataset,
    batch_size=args.batch_size,
    max_target_length=args.max_target_length,
    saving_path=args.saving_path,
)

results = eval.call()
print(results)
